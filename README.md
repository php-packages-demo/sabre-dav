# sabre-dav

WebDAV Framework. https://packagist.org/packages/sabre/dav

# Similar projects
* Using Curl to connect to DAV
  * https://github.com/sabre-dav-demo/simple-server
  * https://github.com/sabre-dav-demo/file-authenticated-webdav